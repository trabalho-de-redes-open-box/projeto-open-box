package org.moonlightcontroller.blocks;

import org.moonlightcontroller.exceptions.MergeException;
import org.moonlightcontroller.processing.BlockClass;
import org.moonlightcontroller.processing.ProcessingBlock;

import java.util.Map;

public class DecIpTtl extends ProcessingBlock implements IStaticProcessingBlock{
	private boolean active;

	public DecIpTtl(String id) {
		super(id);
	}
	
	public DecIpTtl(String id, boolean active) {
		super(id);
		this.active = active;
	}

	public boolean getActive() {
		return active;
	}

	@Override
	public BlockClass getBlockClass() {
		return BlockClass.BLOCK_CLASS_MODIFIER;
	}

	@Override
	protected void putConfiguration(Map<String, Object> config) {
		config.put("active", this.active);
	}

	@Override
	protected ProcessingBlock spawn(String id) {
		return new DecIpTtl(id, this.active);
	}

	@Override
	public boolean canMergeWith(IStaticProcessingBlock other) {
		return false;
	}

	@Override
	public IStaticProcessingBlock mergeWith(IStaticProcessingBlock other) throws MergeException {
		return null;
	}
}
